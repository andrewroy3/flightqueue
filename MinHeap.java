import java.util.ArrayList;

//this file is where the min heap is created
public class MinHeap {

    //constructors
    private ArrayList<Integer> list;

    public MinHeap() {

        this.list = new ArrayList<Integer>();
    }

    public MinHeap(ArrayList<Integer> items) {

        this.list = items;
        buildHeap();
    }
    
    //value is inserted into the list and put into the correct location
    public void insert(int item) {
        list.add(item);
        int i = list.size() - 1;
        int parent = parent(i);
        
        //compares values and parents, swaps if necessary
        while (parent != i && list.get(i) < list.get(parent)) {

            swap(i, parent);
            i = parent;
            parent = parent(i);
        }
    }
    
    //creates a min heap out of the list
    public void buildHeap() {

        for (int i = list.size() / 2; i >= 0; i--) {
            minHeapify(i);
        }
    }
    
    //pulls the minimum value from the heap and re-sorts
    public int extractMin() {
         
        //if the list size is zero, error, if it's one, just take that value
        if (list.size() == 0) {

            throw new IllegalStateException("empty");
        } else if (list.size() == 1) {

            int min = list.remove(0);
            return min;
        }
         
        //take first value from heap, then replace it with last value and call the minHeapify funciton to percolate 
        int min = list.get(0);
        int lastItem = list.remove(list.size() - 1);
        list.set(0, lastItem);

        minHeapify(0);
        return min;
    }
    
    //percolates a value
    private void minHeapify(int i) {

        int left = left(i);
        int right = right(i);
        int smallest = -1;
         
        //compares value with left and then right and swaps if necessary 
        if (left <= list.size() - 1 && list.get(left) < list.get(i)) {
            smallest = left;
        } else {
            smallest = i;
        }

        if (right <= list.size() - 1 && list.get(right) < list.get(smallest)) {
            smallest = right;
        }

        if (smallest != i) {

            swap(i, smallest);
            minHeapify(smallest);
        }
    }
    
    //returns size of heap
    public int getSize(){
    
      return list.size();
    }

    //returns whether heap is empty or not
    public boolean isEmpty() {

        return list.size() == 0;
    }
    
    //to find right child
    private int right(int i) {

        return 2 * i + 2;
    }
      
    //to find left child  
    private int left(int i) {

        return 2 * i + 1;
    }
    
    //to find parent
    private int parent(int i) {

        if (i % 2 == 1) {
            return i / 2;
        }

        return (i - 1) / 2;
    }
    
    //swaps value with parent
    private void swap(int i, int parent) {

        int temp = list.get(parent);
        list.set(parent, list.get(i));
        list.set(i, temp);
    }

}