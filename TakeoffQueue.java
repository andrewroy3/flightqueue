//Andrew Roy CS 145
//7/24/17

//Min heap source: https://gist.github.com/flexelem/70b120ac9bf2965f419f
//the purpose of this program is to organize flight schedules based on passenger counts and time
//can't have a line space in the input file

import java.util.ArrayList;
import java.util.*;
import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;


public class TakeoffQueue{
    public static void main(String[] args){
    //variables declared
      String file = args[0];     
      String prior = args[1];
      String time;
      String passNum; 
      String flight;
      MinHeap minHeap = new MinHeap();
      ArrayList<String> passList = new ArrayList<String>();
      ArrayList<String> flights = new ArrayList<String>();
      ArrayList<String> times = new ArrayList<String>();
      ArrayList<String> nums = new ArrayList<String>();
      int size = 0;
      int count = 0;
      int currentTime = 0;
      int takeOffTime = 0;
      int passInt = 0;
      int timeLastFlight = 0;
      
      //this is for the first prioritization
      //every time a new line is accessed, I add one to the count and then put that into the minheap
      //at the end, I extract the minimums from the heap, in order, and put them into the flightCode function to see what flights they correspond with
      if(prior.equals("1")){ 
            try{
               Scanner data = new Scanner(new File(file));             
               while (data.hasNextLine()) { 
                  count ++;
                  flight = data.next();
                  data.next();
                  data.next();
                  time = data.next();
                  passNum = data.next();
                  
                  //min heap insertion and adding to arrays for flightCode
                  minHeap.insert(count);
                  String order = Integer.toString(count);
                  nums.add(order);
                  flights.add(flight);
                  
               }
      
            } 
            
            catch(FileNotFoundException e){
               System.out.println("Error");
            }  
            
            size = minHeap.getSize();
            
            //extract minimums and find codes
            for(int i = 0; i < size; i++){
              int min = minHeap.extractMin();
              flightCode(nums, flights, min); 
              System.out.println("");
            }   
                        
          
       }
      
      //this is for prioritization 2
      //this time I put the number of passengers into the minHeap and sort them
      //I then create a stack and extract the min's, then pop from the stack so I can get the min's in reverse order,
      //since the planes with the most passengers are going first
      //once I have the passeneger numbers in the correct order I put them into the flightCode function to get the flight they correspond with
      if(prior.equals("2")){ 
         try{
            Scanner data = new Scanner(new File(file));             
            while (data.hasNextLine()) { 
               flight = data.next();
               data.next();
               data.next();
               time = data.next();
               passNum = data.next();
               
               //min heap insertion and adding to arrays for flightCode
               minHeap.insert(Integer.parseInt(passNum));
               passList.add(passNum);
               flights.add(flight);
            }
   
         } 
         
         catch(FileNotFoundException e){
            System.out.println("Error");
         }  

           //stack adding, minheap extracting, and printing
           size = minHeap.getSize(); 
           Stack stk = new Stack();
           
           for(int i = 0; i < size; i++){ 
               stk.push(minHeap.extractMin());
           }
                    
           for(int i = 0; i < size; i++){ 
            Integer mostPass = (int) stk.pop();
            flightCode(passList, flights, mostPass);
            System.out.println(mostPass);
           }
                  
   
           
       }
      
      //this is for prioritization 3
      //this essentially relies upon comparing the time of the last flight with the current time that a plane is requesting take off
      //the time it will take a plane to take off is determined by dividing the passenger count by two, and then by 60, then adding those minutes to the request time
      //once a plane has taken off, that time is saved and compared to the next request time, if the request is before the last take off, that plane is put on hold
      //if another request is also made before the previous take off,
      //both requests are put into the min heap and the one with more passengers will fly once the previous plane has taken off
      //every request made before the last flight has taken off is added to the minheap
      //there are a lot of helper functions here for the numbers that I'll explain later      
      if(prior.equals("3")){
         int count2 = 0;
         Stack stk = new Stack();
         try{
            Scanner data = new Scanner(new File(file));             
            while (data.hasNextLine()) { 
            
               flight = data.next();
               data.next();
               data.next();
               time = data.next();
               currentTime = timeNum(time);
               passNum = data.next();
               passInt = Integer.parseInt(passNum);
               
               passList.add(passNum);
               flights.add(flight);
               times.add(time);               
               
               //if the time of last flight is greater than or equal to the current request time, the plane is automatically put into the min heap,
               //based on its passeneger count
               if(timeLastFlight >= currentTime){
                  minHeap.insert(passInt);
               }
               
               //if the request time is greater than the last flight time, but there are other planes in the min heap,
               //then I cycle through to make sure the ones with the most passengers fly first
               if((timeLastFlight < currentTime) & (minHeap.isEmpty() == false)){
                 size = minHeap.getSize(); 
                 
                 //extract from min heap and put into reverse order
                 for(int i = 0; i < size; i++){ 
                     stk.push(minHeap.extractMin());
                 }
                 
                 //I print the times that each plane is taking off
                 //this stops once the flight time of one of the planes surpasses the request time of the current plane or the stack is empty   
                 while((timeLastFlight < currentTime) & (stk.isEmpty() == false)){           
                  count2 ++;       
                  Integer mostPass = (int) stk.pop();
                  flightCode(passList, flights, mostPass);
                  takeOffTime = round(mostPass) + timeLastFlight;                 
                  System.out.print("departed at ");
                  correctNum(takeOffTime);
                  timeLastFlight = takeOffTime;
                 }
                 //if the current plane made its request before a plane in the stack flew then it is also put into the minheap
                 if(timeLastFlight >= currentTime){
                  minHeap.insert(passInt);
                 }
                 //any remaining planes are re-added to the minheap
                 for(int i = 0; i < stk.size(); i++){
                  Integer stkInsrt = (int) stk.pop();
                  minHeap.insert(stkInsrt);
                 }
                     
               }
               
               //if there's no planes waiting (heap is empty) then the current plane flies immediately               
               if(minHeap.isEmpty() == true){
                  takeOffTime = round(passInt) + currentTime;
                  flightCode(passList, flights, passInt);
                  System.out.print(" departed at ");
                  correctNum(takeOffTime);
                  timeLastFlight = takeOffTime;
               }
               
            }
   
         } 
         
         catch(FileNotFoundException e){
            System.out.println("Error");
         } 
         
         //these are here for the leftover planes in the min heap after the file is finished parsing
         while(minHeap.isEmpty() == false){
              stk.push(minHeap.extractMin());
         }  
          
         
         while(stk.isEmpty() == false){
             Integer mostPass = (int) stk.pop();
             flightCode(passList, flights, mostPass);
             takeOffTime = round(mostPass) + timeLastFlight;                 
             System.out.print("departed at ");
             correctNum(takeOffTime);
             timeLastFlight = takeOffTime;
         }             

      }
    }
    
    //the list of passeneger counts and flight codes is passed in, along with the passeneger count of the plane being searched for
    //index of the desired passeneger count is found, this will be the same as the index of the correct flight code since they were added at the same time
    //duplicate passeneger counts are handled by immediately replacing the passeneger count with "done", so this index is not found again
    public static void flightCode(ArrayList<String> passList, ArrayList<String> flights, int pass){
      String pass2 = Integer.toString(pass);
      int index = passList.indexOf(pass2);
      passList.set(index, "done");    
      System.out.print(flights.get(index)+" ");          
    }
    
    //the goal of this function is to change the times into easily comparable numbers
    //i.e. 1:05 = 1265, 3:42 = 1422. each minute after 1260 is counted as +1 to 1259
    public static int timeNum(String time){
      int dig = 0;
      String digA;
      String digB;
      String digC;
      String digD;
      int digF = 0;
      int digG = 0;
      
      //if it's a 4 digit time
      if(time.length() > 4){
         digA = time.substring(0,2);
         digB = time.substring(3,5);
         
         //if it's not a 12 o' clock time I calculate how many minutes after 1:00 it is
         if(digA.equals("12") == false){
            digF = ((Integer.parseInt(digA)-1)*60)+1260;
            dig = digF + Integer.parseInt(digB);
         }
         else{
            digC = digA + digB;
            dig = Integer.parseInt(digC);
         }          
                   
      }
      
      //if it's a 3 digit time
      else{
         digA = time.substring(0,1);
         digB = time.substring(2,4);
         
         digF = ((Integer.parseInt(digA)-1)*60)+1260;
         dig = digF + Integer.parseInt(digB);
                         
      }      
      
      return dig;
    }
    
    //this calculates the take off time which is added to the request time above
    public static int round(int passInt){
      double passDbl = passInt/2.0;
      passDbl = Math.ceil(passDbl/60.0);
      
      passInt = (int) passDbl;
      return passInt;
    }
    
    //this takes one of the comparing integers and turns it back into the time it actually is for printing
    //it's basically just a reverse of the timeNum function
    public static void correctNum(int time){
      String a = Integer.toString(time);
      String digA;
      String digB;
      String digC;
      int time2 = 0;
      int time3 = 0;
      int timeInt = 0;
      
      //time 2 calculates the hour by dividing the time after 1:00 by 60
      //time 3 caluclates the minutes by using mod on the time and looking at the remainder
      //they are then added together and printed as a time
      if(time > 1259){
         timeInt = time - 1200;
         time2 = timeInt/60;
         time3 = timeInt % 60;
         
         if(time3 < 10){
            a = Integer.toString(time2)+"0"+Integer.toString(time3);
         }
         else{
            a = Integer.toString(time2)+Integer.toString(time3);
         }   
         //look if time is after 9:59 or not
         if(time < 1800){
            System.out.println(a.substring(0,1)+":"+a.substring(1,3));
         }   
         else{
            System.out.println(a.substring(0,2)+":"+a.substring(2,4));

         }
      }
      
      //just print for 12 o' clock times
      else{
         System.out.println(a.substring(0,2)+":"+a.substring(2,4));
      }
    }
}    
    