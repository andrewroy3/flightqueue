Takes in a flight ID, a time of departure, and a number of passengers.
It then calculates take off time based on the number of passengers.
If a flights take off time overlaps with another flights desired take off time, then the latter will be put on hold (in a min heap) and any other planes that want to take off before the previous flight will also be put in the heap.
This minheap can get fairly large as times continue to overlap.